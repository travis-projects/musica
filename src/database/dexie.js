import Dexie from 'dexie'

export const db = new Dexie('MusicDatabase')
db.version(1).stores({ files: '++id,uid,path,mime,title,type,tags' })

const dbFunctions = {
  ...db
}

const DB = {
  install (Vue, options) {
    Vue.prototype.$DB = dbFunctions
  }
}

export default DB
