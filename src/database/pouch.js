import PouchDB from 'pouchdb-browser'

export const db = new PouchDB('musica')

const PDB = {
  install (Vue, options) {
    Vue.prototype.$PDB = db
  }
}

export default PDB
