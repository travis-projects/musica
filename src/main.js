import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import db from './database/dexie'
import pdb from './database/pouch'
import Buefy from 'buefy'
import VuePlyr from 'vue-plyr'

import './assets/scss/app.scss'
import 'typeface-open-sans'
import 'typeface-poppins'
import 'bulma-slider/dist/js/bulma-slider'

Vue.use(VuePlyr)
Vue.use(Buefy)
Vue.use(db)
Vue.use(pdb)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
