import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/add',
      name: 'add',
      component: () => import(/* webpackChunkName: "add" */ './views/AddFiles')
    },
    {
      path: '/explore',
      name: 'explore',
      component: () => import(/* webpackChunkName: "explore" */ './views/Explore')
    },
    {
      path: '/play',
      name: 'play',
      component: () => import(/* webpackChunkName: "play" */ './views/Play')
    }
  ]
})
