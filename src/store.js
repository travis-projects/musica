import Vue from 'vue'
import Vuex from 'vuex'
import { db } from './database/dexie'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    files: [],
    player: {
      currentTrack: {},
      queue: [],
      playing: false,
      currentTime: ''
    }
  },
  mutations: {
    setFiles (state, files) {
      state.files = files
    },
    setCurrentTrack (state, file) {
      state.player.currentTrack = file
    },
    setPlaying (state, isPlaying) {
      state.player.playing = isPlaying
    },
    setQueue (state, queue) {
      state.player.queue = queue
    }
  },
  actions: {
    async loadFiles ({ commit }) {
      const files = await db.files.toArray()
      commit('setFiles', files)
    },
    addFiles ({ state, commit }, newFiles) {
      const files = state.files
      files.unshift(...newFiles)
      commit('setFiles', files)
    },
    setPlaying ({ state, commit }, file) {
      if (state.player.currentTrack.uid !== file.uid) commit('setCurrentTrack', file)
      commit('setPlaying', true)
    },
    setPause ({ commit }) {
      commit('setPlaying', false)
    },
    changeCurrentTrack ({ state, commit }) {
      const queue = state.player.queue
      commit('setCurrentTrack', queue[0])
      const newQueue = queue
      newQueue.shift()
      commit('setQueue', newQueue)
    },
    addToQueue ({ state, commit }, file) {
      const queue = state.player.queue
      queue.push(file)
      commit('setQueue', queue)
    }
    // removeFromQueue ({ state, commit }, file) {
    //   const queue = state.player.queue
    //   const newQueue = queue.filter(song => song.uid !== file.id)
    //   commit('setQueue', newQueue)
    // }
  }
})
